// **** Libraries to Include for correct working of Rocker ****

#include <Arduino.h>
#include <avr/io.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <ILI9225_kbv.h>
#include "EuroStile12x11.h"
#include "EuroStyle 13x11.h"
#include <avr/interrupt.h>
#include <Keypad.h>
#include <DRV8825.h>


// **** Declaration of Stepper Motor variables ***

#define MOTOR_STEPS 200
#define DIR 5
#define STEP 6
#define MODE0 A5
#define MODE1 A4
#define MODE2 A3
DRV8825 stepper(MOTOR_STEPS, DIR, STEP, MODE0, MODE1, MODE2);
// **** Declaration of Keypad Variables ****

const byte ROWS = 1; //one row
const byte COLS = 4; //four columns
//define the cymbols on the buttons of the keypads
char hexaKeys[ROWS][COLS] = {
  {'4', '2', '1', '3'}
};
byte rowPins[ROWS] = {1}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {0, 2, 3, 4}; //connect to the column pinouts of the keypad
Keypad keypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

int FirstTime = 0;
//int TotalSteps = 1110; // number of steps with 1/4 step resolution.
//int StepsInAngle = 47;

// **** declaration of LCD TFT variables ****

//#define TFT_LED 7   // 0 if wired to +5V directly
#define TFT_RST 8   // Reset
#define TFT_CS  10  // Chip Select
#define TFT_RS  9   //
#define TFT_CLK 13  // Serial Clock
#define TFT_SDI 11  // Serial Data In
ILI9225_kbv tft (TFT_CS, TFT_RS, TFT_RST); // Make use of hardware SPI--> Much quicker in this case.

// **** definition of TFT Colors ****

#define BLACK 0x0000
#define GREEN 0x9640
#define RED 0xF800
#define YELLOW 0xFFE0
#define BLUE 0x059D
#define DGREEN 0x2965

// **** declaration of general variables ****//

String String0, String1, String2, String3;
int x1, y1, w, h, y, i, f, s;
int d;
int p = 1; // pointer for Seconds Array
int Angle = 7;
int Minutes = 8;
int Seconds = 0;
long RunInterval, RunInterval2, StepInterval, StepInterval2, StartStepTime, StartRunTime;
int TextColor;
char keys;
int StepsInAngle = 47;
String ModeString = "INTERVAL";
bool Intervalmode = true;
bool Running = false;
bool Cursor = false;
int SelectFunction, ScreenIndex = 0;
int Backlight = 1;

long CurrentMillis = 0;
long Displaymillis = 0;

void setup() {

  //set Analog inputs to behave like Digital inputs for KEYPAD

  pinMode(0, INPUT_PULLUP);
  pinMode(1, INPUT_PULLUP);
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(7, OUTPUT);
  pinMode(12, OUTPUT);

  //set Digital Inputs to read IR Sensors.

  pinMode(A1, INPUT_PULLUP); //Sensor Right
  pinMode(A2, INPUT_PULLUP); //Sensor Left
  pinMode(A5, OUTPUT);
  pinMode(A3, OUTPUT);
  pinMode(A4, OUTPUT);

  stepper.begin();
  stepper.setMicrostep(8);

  // 1/16 step = 12V  | 0,215 A = 3W.
  // 1/8  step = 12V  | 0,220 A
  // 1/4  step = 12V  | 0,220 A

  // Turn on TFT screen
  keypad.addEventListener(keypadEvent);
  tft.begin();
  tft.setRotation(3);
  tft.setFont(&Orbitron_Medium_17);
  digitalWrite(7, HIGH);
  TextColor = GREEN;
  ModeString = "INTERVAL";
  i = 0;
  SelectMenu(ScreenIndex);

}

void loop() {

  keys = keypad.getKey();

}

//************************************************ Calibration Procedure *************************************************//
// The stepper motor first turns to the left until the sensor is sensing the side of the rocker                           //
// Then the stepper motor turns a full turn to the right, while counting the steps.                                       //
// When the right sensor sensed the rocker, the stepper then turns half of the counted steps back to the middle position  //
//************************************************************************************************************************//

int Calibration() {

  int i = 0;
  Running = false;
  tft.fillScreen(BLACK);
  ShowText2("Calibrating", 100, TextColor); //Show Text "Calibrating"
  int SensorLeft = digitalRead(A1);
  int SensorRight = digitalRead(A2);
  if (SensorRight != 1 ) {

    do
    {
      stepper.move(1);
      delay(4);
      SensorRight = digitalRead(A2);

    } while (SensorRight == 0);
    i = 0;
    do
    {
      stepper.move(-1);
      delay(4);
      i = i + 1;
      SensorLeft = digitalRead(A1);
    } while (SensorLeft == 0);

    i = i / 2;
    i = i - 30;
    for (d = 0; d <= i; d++)
    {
      stepper.move(1);
      delay(4);
    }


  }
  else
  {

    do
    {
      stepper.move(-1);
      delay(4);
      SensorLeft = digitalRead(A1);

    } while (SensorLeft == 0);
    i = 0;
    do
    {
      stepper.move(1);
      delay(4);
      i = i + 1;
      SensorRight = digitalRead(A2);
    } while (SensorRight == 0);

    i = i / 2;
    i = i + 45;
    for (d = 0; d <= i; d++)
    {
      stepper.move(-1);
      delay(4);
    }

  }


  ShowText2("Calibrating", 100, BLACK);
  tft.fillScreen(TextColor);

}


void ShowText2 (String stringtext, int y, unsigned int colour) {
  int x;
  tft.getTextBounds(stringtext, x, y, &x1, &y1, &w, &h);
  x = 91 - (w / 2);
  tft.setCursor(x, y);
  tft.setTextColor(colour);
  tft.print(stringtext); // Print string

}
void ShowText(String stringtext, int x, int y, unsigned int colour) {

  tft.getTextBounds(stringtext, x, y, &x1, &y1, &w, &h);
  tft.setCursor(x, y);
  tft.setTextColor(colour);
  tft.print(stringtext); // Print string

}

void SelectMenu(int SelectFunction) {

  if (SelectFunction == 0) {
    tft.fillScreen(GREEN);
    tft.setFont(&Orbitron_Bold_13);
    tft.setTextSize(2);
    ShowText2("!", 30, BLACK); //Draw the middle green section including the words Mimetas OrganoFlow
    tft.setTextSize(1);
    ShowText2("REMOVE ALL", 52, BLACK); //Draw the middle green section including the words Mimetas OrganoFlow
    ShowText2("ORGANOPLATES", 81, BLACK); //Draw the middle green section including the words Mimetas OrganoFlow
    tft.fillRect(1, 92, 190, 41, BLACK);
    tft.drawFastHLine(5, 91, 177, 0x2100);
    tft.drawFastHLine(17, 133, 153, 0x2100);
    ShowText2("CALIBRATE   >", 118, GREEN); //Draw the middle green section including the words Mimetas OrganoFlowtft.fillScreen(GREEN);
    ShowText2("V1.B1", 154, BLACK); // SoftWareVersion 4.7

    tft.setFont(&Orbitron_Medium_17);

  }

  else if (SelectFunction == 1) {

    //Screen 1: = StartScreen
    tft.fillRect(1, 84, 190, 82, TextColor);
    ShowAngle(Angle, BLACK, false);
    tft.drawCircle(62, 66, 2, BLACK);
    ShowMinutes(Minutes, false);
    ShowText(":", 129, 110, BLACK);
    ShowSeconds(Seconds, false);
    ShowText2("START", 140, BLACK);
    tft.fillRect(1, 54, 190, 41, BLACK);
    ShowText2(ModeString + " >", 80, TextColor);

  }
  else if (SelectFunction == 2)
  {
    tft.fillScreen(GREEN);
    TextColor = GREEN;
    tft.fillRect(1, 54, 190, 41, BLACK);
    ModeString = "INTERVAL";
    ShowText2((ModeString + " >"), 80, TextColor);
    ShowText2("CONTINUOUS", 110, BLACK);
    Intervalmode = true;
    Angle = 7;
    Minutes = 8;
    Seconds = 0;
  }
  else if (SelectFunction == 3)
  {
    // Screen 3 = MainScreen
    tft.fillRect(1, 30, 190, 61, TextColor);
    tft.fillRect(1, 114, 190, 41, TextColor);
    tft.fillRect(1, 84, 190, 41, BLACK);
    ShowAngle(Angle, TextColor, false);
    ShowMinutes(Minutes, false);
    ShowSeconds(Seconds, false);
    ShowText(":", 129, 110, TextColor);
    ShowText(">", 164, 110, TextColor);
    ShowText2(ModeString, 80, BLACK);
    ShowText2("START", 140, BLACK);
  }
  else if (SelectFunction == 4)
  {
    // Screen 4 = Set Angle
    tft.fillRect(90, 84, 90, 41, BLACK);
    ShowText2(ModeString, 80, TextColor);
    ShowAngle(Angle, TextColor, true);
    ShowText2("START", 140, TextColor);
    ShowMinutes(Minutes, false);
    ShowText(":", 129, 110, DGREEN);
    ShowSeconds(Seconds, false);


  }
  else if (SelectFunction == 5)
  {
    // Screen 5: = Set Minutes
    ShowMinutes(Minutes, true);

  }
  else if (SelectFunction == 6)
  {
    // Screen 6: = Set Seconds

    ShowSeconds(Seconds, true);
  }
  else if (SelectFunction == 7)
  {
    // Screen 7: = Screen ready to Rock
    Displaymillis = millis();
    SelectStart();
  }
  else if (SelectFunction == 8)
  {
    tft.fillScreen(BLACK);
    ShowText2(ModeString, 80, TextColor);
    ShowText(String(Angle), 30, 110, TextColor);
    tft.drawCircle(62, 100, 2, TextColor);
    //ShowMinutes(Minutes, false);
    ShowSeconds(Seconds, false);
    ShowText2("ROCKING...", 128, TextColor);
    if (Running == false) {
      Running = true;
      RunProgram();
    }

  }
  else if (SelectFunction == 9)
  {
    tft.fillRect(1, 125, 190, 33, RED);
    tft.fillRect(1, 85, 190, 33, BLACK);
    ShowText2("STOP", 35, BLACK);
    ShowText2("PROGRAM?", 65, BLACK);
    ShowText2("NO >", 105, RED);
    ShowText2("YES  ", 145, BLACK);
    Running = true;


  }
  else if (SelectFunction == 10)
  {
    tft.fillRect(1, 85, 190, 33, RED);
    tft.fillRect(1, 125, 190, 33, BLACK);
    ShowText2("NO  ", 105, BLACK);
    ShowText2("YES >", 145, RED);
    Running = true;

  }
  else if (SelectFunction == 12)
  {
    tft.fillScreen(BLUE);
    TextColor = BLUE;
    ShowText2("INTERVAL", 80, BLACK);
    tft.fillRect(1, 84, 190, 41, BLACK);
    ShowText2("CONTINUOUS >", 110, TextColor);
    ModeString = "CONTINUOUS";
    Intervalmode = false;
    Angle = 14;                 //changed for testing purposed, was angle 14, minute 1
    Minutes = 1;
    Seconds = 0;
  }

}

void SelectStart()
{
  tft.fillRect(1, 21, 190, 102, TextColor);

  ShowAngle(Angle, BLACK, false);
  ShowText2(ModeString, 80, BLACK);
  ShowMinutes(Minutes, false);
  ShowText(":", 129, 110, BLACK);
  ShowSeconds(Seconds, false);
  tft.fillRect(1, 114, 190, 41, BLACK);
  ShowText2("START >", 140, TextColor);

}

void ShowMinutes(int NewMinutes, bool selected)
{

  if (NewMinutes == 0) {

    String3 = "0" + String(NewMinutes + 2);
    String2 = "0" + String(NewMinutes + 1);
    String1 = "0" + String(NewMinutes);
    String0 = "";
    NewMinutes = 0;

  }
  else if (NewMinutes >= 1 && NewMinutes <= 7) {

    String3 = "0" + String(NewMinutes + 2);
    String2 = "0" + String(NewMinutes + 1);
    String1 = "0" + String(NewMinutes);
    String0 = "0" + String(NewMinutes - 1);

  }
  else if (NewMinutes == 8) {

    String3 = String(NewMinutes + 2);
    String2 = "0" + String(NewMinutes + 1);
    String1 = "0" + String(NewMinutes);
    String0 = "0" + String(NewMinutes - 1);

  }
  else if (NewMinutes == 9) {

    String3 = String(NewMinutes + 2);
    String2 = String(NewMinutes + 1);
    String1 = "0" + String(NewMinutes);
    String0 = "0" + String(NewMinutes - 1);

  }
  else if (NewMinutes == 10) {

    String3 = String(NewMinutes + 2);
    String2 = String(NewMinutes + 1);
    String1 = String(NewMinutes);
    String0 = "0" + String(NewMinutes - 1);

  }
  else if (NewMinutes >= 11 && NewMinutes <= 28) {


    String3 = String(NewMinutes + 2);
    String2 = String(NewMinutes + 1);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 1);

  }
  else if (NewMinutes == 29) {

    String3 = String(NewMinutes + 6);
    String2 = String(NewMinutes + 1);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 1);

  }
  else if (NewMinutes == 30) {

    String3 = String(NewMinutes + 10);
    String2 = String(NewMinutes + 5);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 1);

  }
  else if (NewMinutes >= 35 && NewMinutes <= 110) {

    String3 = String(NewMinutes + 10);
    String2 = String(NewMinutes + 5);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 5);

  }
  else if (NewMinutes == 115) {

    String3 = String(NewMinutes + 15);
    String2 = String(NewMinutes + 5);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 5);

  }
  else if (NewMinutes == 120) {


    String3 = String(NewMinutes + 20);
    String2 = String(NewMinutes + 10);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 5);

  }

  else if (NewMinutes >= 130  && NewMinutes <= 970) {


    String3 = String(NewMinutes + 20);
    String2 = String(NewMinutes + 10);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 10);


  }
  else if (NewMinutes == 980) {

    String3 = "";
    String2 = String(NewMinutes + 10);
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 10);

  }
  else if (NewMinutes == 990) {

    String3 = "";
    String2 = "";
    String1 = String(NewMinutes);
    String0 = String(NewMinutes - 10);

  }


  if (ScreenIndex == 5)
  {
    ShowText(">", 70, 110, BLACK);
    tft.fillRect(1, 1, 190, 82, TextColor);
    tft.fillRect(1, 125, 190, 41, TextColor);
    tft.fillRect(85, 84, 50, 41, BLACK);
    ShowAngle(Angle, DGREEN, false);
    ShowText(":", 129, 110, DGREEN);

    ShowText(">", 164, 110, TextColor);
    ShowText(String3, 85, 46, DGREEN);
    ShowText(String2, 85, 80, DGREEN);
    ShowText(String1, 85, 110, TextColor);
    ShowText(String0, 85 , 144, DGREEN);

  }
  if (selected == true) {
    ShowText(String1, 85, 110, TextColor);
  }
  else if (selected == false)
  {
    ShowText(String1, 85, 110, BLACK);
  }
  if (ScreenIndex == 3 || ScreenIndex == 8) {
    ShowText(String1, 85, 110, TextColor);
  }
  if (ScreenIndex == 4 || ScreenIndex == 6) {
    ShowText(String1, 85, 110, DGREEN);
  }

  Minutes = NewMinutes;

}

void ShowSeconds(int NewSeconds, bool selected)
{
  ShowMinutes(Minutes, false);

  if (NewSeconds == 0)
  {
    String3 = String(NewSeconds + 10);
    String2 = "0" + String(NewSeconds + 5);
    String1 = "0" + String(NewSeconds);
    String0 = "";
    if (Intervalmode == false && Minutes == 0)
    {
      NewSeconds = 5;
      String3 = String(NewSeconds + 10);
      String2 = String(NewSeconds + 5);
      String1 = "0" + String(NewSeconds);
    }

  }

  else if (NewSeconds == 5)
  {
    String3 = String(NewSeconds + 10);
    String2 = String(NewSeconds + 5);
    String1 = "0" + String(NewSeconds);
    if (Intervalmode == false && Minutes == 0) {
      String0 = "";
    }
    else
    {
      String0 = "0" + String(NewSeconds - 5);
    }
  }
  else if (NewSeconds == 10) {
    String3 = String(NewSeconds + 10);
    String2 = String(NewSeconds + 5);
    String1 = String(NewSeconds);
    String0 = "0" + String(NewSeconds - 5);
  }


  else if (NewSeconds >= 10 && NewSeconds <= 45) {
    String3 = String(NewSeconds + 10);
    String2 = String(NewSeconds + 5);
    String1 = String(NewSeconds);
    String0 = String(NewSeconds - 5);
  }
  else if (NewSeconds == 50) {
    String3 = "";
    String2 = String(NewSeconds + 5);
    String1 = String(NewSeconds);
    String0 = String(NewSeconds - 5);
  }
  else if (NewSeconds == 55) {

    String3 = "";
    String2 = "";
    String1 = String(NewSeconds);
    String0 = String(NewSeconds - 5);
  }

  if (selected == true) {

    ShowText(":", 129, 110, TextColor );
    ShowText(">", 164, 110, TextColor);
    tft.fillRect(1, 1, 190, 82, TextColor);
    tft.fillRect(1, 125, 190, 41, TextColor);
    tft.fillRect(134, 84, 31, 41, BLACK);
    ShowText(String3, 134, 46, DGREEN);
    ShowText(String2, 134, 80, DGREEN);
    ShowText(String1, 134, 110, TextColor);
    ShowText(String0, 134, 144, DGREEN);

  }
  else if (selected == false)
  {
    ShowText(String1, 134, 110, BLACK);
  }
  if (ScreenIndex == 3) {

    ShowText(String1, 134, 110, TextColor);
  }
  if (ScreenIndex == 4 || ScreenIndex == 5) {

    ShowText(String1, 134, 110, DGREEN);
  }
  if (ScreenIndex == 8)
  {
    ShowText(String1, 134, 110, TextColor);
    ShowText(":", 129, 110, TextColor );
    ShowText(">", 164, 110, BLACK);
  }

  Seconds = NewSeconds;
}

void ShowAngle(int NewAngle, unsigned int colour, bool Cursor)
{

  if (Cursor == true) {

    tft.fillRect(1, 1, 190, 82, TextColor);
    tft.fillRect(1, 125, 190, 41, TextColor);
    ShowText(String(Angle), 30, 110, BLACK);
    tft.drawCircle(62, 100, 2, colour);
    ShowText(">", 70, 110, TextColor);

    if (NewAngle == 1)
    {
      ShowText(String(NewAngle + 2), 30, 46, DGREEN);
      ShowText(String(NewAngle + 1), 30, 80, DGREEN);
      tft.drawCircle(62, 32, 2, DGREEN);
      tft.drawCircle(62, 66, 2, DGREEN);

    }
    else if (NewAngle >= 2 && NewAngle <= 23)
    {
      ShowText(String(NewAngle + 2), 30, 46, DGREEN);
      ShowText(String(NewAngle + 1), 30, 80, DGREEN);
      ShowText(String(NewAngle - 1), 30 , 144, DGREEN);
      tft.drawCircle(62, 32, 2, DGREEN);
      tft.drawCircle(62, 66, 2, DGREEN);
      tft.drawCircle(62, 135, 2, DGREEN);
    }
    else if (NewAngle == 24)
    {
      ShowText(String(NewAngle + 1), 30, 80, DGREEN);
      ShowText(String(NewAngle - 1), 30 , 144, DGREEN);
      tft.drawCircle(62, 66, 2, DGREEN);
      tft.drawCircle(62, 135, 2, DGREEN);
    }
    else if (NewAngle == 25)
    {
      ShowText(String(NewAngle - 1), 30 , 144, DGREEN);
      tft.drawCircle(62, 135, 2, DGREEN);
    }

    ShowText(String(NewAngle), 30, 110, TextColor);
    Angle = NewAngle;
  }

  else
  {
    tft.drawCircle(62, 100, 2, colour);
    ShowText(String(Angle), 30, 110, colour);
  }
}

void RunProgram() {

  int TotalSteps = Angle * StepsInAngle;              //  47 is the total steps per Angle.
  FirstTime = 0;
  if ((Minutes + Seconds) <= 500)
  {
    RunInterval = Seconds + (Minutes * 60);
    RunInterval = RunInterval * 1000;
    RunInterval2 = 0;
    StepInterval = RunInterval / TotalSteps;
    StepInterval2 = 0;
  }
  else if ((Minutes + Seconds) > 505)
  {

    RunInterval = Seconds + ((Minutes - 500) * 60);
    RunInterval = RunInterval * 1000;
    RunInterval2 = 30000000;
    StepInterval = RunInterval / TotalSteps;
    StepInterval2 = 30000000 / TotalSteps;
  }
  Running = true;

  if (Intervalmode == true) {
    d = -1;
    if (FirstTime == 0 && Running == true) {           //Run Program while no button is pressed and running equals true.

      for (int i = 0; i < (TotalSteps / 2); i++)                      //Take number of Motor steps based on Angle * Steps in Angle.
      {
        CurrentMillis = millis();                           // Take StartTime in Milliseconds
        stepper.move(d);                                    // Move stepper motor with 1 step
        while (millis() < CurrentMillis + 5)                // wait until Current Time matches (StartTime + 5 milliseconds) for smooth movement.
        {
          keypad.getKey();
          if (Displaymillis + 23000 < millis())
          {
            digitalWrite(7, 125);
            Backlight = 0;
            ScreenIndex = 8;
          }
          if (Running == false)
          {
            break;
          }
        }
      }
      FirstTime = 1;
      d = 1;
    }
    while (FirstTime == 1 && Running == true)
    {
      StartRunTime = millis();
      while (millis() < StartRunTime + RunInterval + RunInterval2)
      {
        keypad.getKey();
        if (Displaymillis + 21000 < millis())
        {
          digitalWrite(7, 125);
          Backlight = 0;
          ScreenIndex = 8;
        }
        if (Running == false)
        {
          break;
        }
      }
      if (Running == false)
      {
        break;
      }

      for (i = 0; i < TotalSteps; i++)                      //Take number of Motor steps based on Angle * Steps in Angle.
      {
        CurrentMillis = millis();                           // Take StartTime in Milliseconds
        stepper.move(d);                                    // Move stepper motor with 1 step
        while (millis() < CurrentMillis + 5)                // wait until Current Time matches (StartTime + 5 milliseconds) for smooth movement.
        {
          keypad.getKey();
          if (Displaymillis + 21000 < millis())
          {
            digitalWrite(7, 125);
            Backlight = 0;
            ScreenIndex = 8;
          }
          if (Running == false)
          {
            break;
          }
        }
        if (Running == false)
        {
          break;
        }
      }
      if (Running == false)
      {
        break;
      }
      if (d == 1)                                           // Change Direction (with variable 'd') of the Motor. If 'd' equals 1 (left), make 'd' -1 (right).
      {
        d = -1;
      }
      else if (d == -1)
      {
        d = 1;
      }
    }
  }
  else if (Intervalmode == false)
  {

    d = -1;
    while (FirstTime == 0 && Running == true)
    {
      
      for (i = 0; i < (TotalSteps / 2); i++) {
        StartStepTime = millis();
        stepper.move(d);
        while (millis() < (StartStepTime + StepInterval + StepInterval2))
        {
          keypad.getKey();
          if (Displaymillis + 23000 < millis())
          {
            digitalWrite(7, 125);
            Backlight = 0;
            ScreenIndex = 8;
          }
          if (Running == false)
          {
            break;
          }
        }

        if (Running == false)
        {
          break;
        }
        
      }
      FirstTime = 1;
      
      if (Running == false)
      {
          break;
      }

    }
    
    d = 1;
    while (FirstTime == 1 && Running == true)
    {
      for (i = 0; i <= TotalSteps; i++)
      {
        StartStepTime = millis();
        stepper.move(d);
        while (millis() < (StartStepTime + StepInterval + StepInterval2))
        {
          keypad.getKey();
          if (Displaymillis + 20000 < millis())
          {
            digitalWrite(7, 125);
            Backlight = 0;
            ScreenIndex = 8;
          }
          if (Running == false)
          {
            break;
          }

        }
        if (Running == false)
        {
          break;
        }

      }
      if (Running == false)
      {
        break;
      }
      if (d == 1)
      {
        d = -1;
      }
      else if (d == -1)
      {
        d = 1;
      }
      
    }
  }
}





void keypadEvent(KeypadEvent key) {
  switch (keypad.getState()) {
    case PRESSED:
      if (key == '1') {
        if (ScreenIndex >= 0 && ScreenIndex <= 7 ) {
          if (ScreenIndex == 0) {
            Calibration();
          }
          ScreenIndex = ScreenIndex + 1;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 8) {
          if (Backlight == 0) {
            SelectMenu(ScreenIndex);
          }
          digitalWrite(7, HIGH);
          Backlight = 1;
          Displaymillis = millis();
        }
        else if (ScreenIndex == 9)
        {
          ScreenIndex = 8;
          Displaymillis = millis();
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 10)
        {
          Running = false;
          ScreenIndex = 0;
          FirstTime = 0;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 12)
        {
          ScreenIndex = 3;
          ModeString = "CONTINUOUS";
          Angle = 14;
          Minutes  = 1;
          Seconds = 0;
          SelectMenu(ScreenIndex);
        }
      }

      if (key == '2') {
        if (ScreenIndex == 1) {
          ScreenIndex = 3;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 2) {
          ScreenIndex = 12;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 3) {
          ScreenIndex = 7;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 4) {
          if (Angle >= 2) {
            ShowAngle(Angle - 1, TextColor, true);
          }
        }
        else if (ScreenIndex == 5) {
          if (Minutes == 0)
          {
            ShowMinutes(Minutes, true);
          }
          if (Minutes >= 1 && Minutes <= 30)
          {
            ShowMinutes(Minutes - 1, true);
          }
          else if (Minutes >= 35 && Minutes <= 115)
          {
            ShowMinutes(Minutes - 5, true);
          }
          else if (Minutes >= 120 && Minutes <= 990)
          {
            ShowMinutes(Minutes - 10, true);
          }
        }
        else if (ScreenIndex == 6) {
          if (Seconds == 0)
          {
            ShowSeconds(Seconds, true);
          }

          if (Seconds >= 5 && Seconds <= 55)
          {
            ShowSeconds(Seconds - 5, true);
          }
        }
        else if (ScreenIndex == 7) {
          ScreenIndex = 1;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 8) {
          if (Backlight == 0) {
            SelectMenu(ScreenIndex);
          }
          digitalWrite(7, HIGH);
          Backlight = 1;
          Displaymillis = millis();

        }
        else if (ScreenIndex == 9) {
          Displaymillis = millis();
          ScreenIndex = 10;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 10) {
          Displaymillis = millis();
          ScreenIndex = 9;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 12) {
          ScreenIndex = 2;
          SelectMenu(ScreenIndex);
        }

      }
      if (key == '3') {
        if (ScreenIndex == 1) {
          ScreenIndex = 7;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 2) {
          ScreenIndex = 12;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 3) {
          ScreenIndex = 1;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 4) {
          if (Angle <= 24)
          {
            ShowAngle(Angle + 1, TextColor, true);
          }
        }
        else if (ScreenIndex == 5) {
          if (Minutes >= 0 && Minutes <= 29)
          {
            ShowMinutes(Minutes + 1, true);
          }
          else if (Minutes >= 30 && Minutes <= 115)
          {
            ShowMinutes(Minutes + 5, true);
          }
          else if (Minutes >= 120 && Minutes <= 980)
          {
            ShowMinutes(Minutes + 10, true);
          }
        }
        else if (ScreenIndex == 6) {
          if (Seconds >= 0 && Seconds <= 50)
          {
            ShowSeconds(Seconds + 5, true);
          }
          else if (Seconds == 55)
          {
            ShowSeconds(Seconds, true);
          }

        }
        else if (ScreenIndex == 7) {
          ScreenIndex = 3;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 8) {
          if (Backlight == 0) {
            SelectMenu(ScreenIndex);
          }
          digitalWrite(7, HIGH);
          Backlight = 1;
          Displaymillis = millis();
        }

        else if (ScreenIndex == 9) {
          Displaymillis = millis();
          ScreenIndex = 10;
          SelectMenu(ScreenIndex);
        }

        else if (ScreenIndex == 10) {
          Displaymillis = millis();
          ScreenIndex = 9;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 12) {
          ScreenIndex = 2;
          SelectMenu(ScreenIndex);
        }
      }
      if (key == '4') {
        if (ScreenIndex == 2) {
          ScreenIndex = 1;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 8) {
          if (Backlight == 0)
          {
            SelectMenu(ScreenIndex);
            digitalWrite(7, HIGH);
            Backlight = 1;
            Displaymillis = millis();
          }
          else if (Backlight == 1)
          {
            tft.fillScreen(0xF800);
            ScreenIndex = 9;
            SelectMenu(ScreenIndex);
          }
        }
        else if (ScreenIndex >= 4 && ScreenIndex <= 6) {
          ScreenIndex = ScreenIndex - 1;
          SelectMenu(ScreenIndex);
        }
        else if (ScreenIndex == 12) {
          ScreenIndex = 1;
          SelectMenu(ScreenIndex);
        }
      }
  }
}
